<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'index']);
Route::get('/admin', [App\Http\Controllers\AdminController::class, 'index'])->middleware('admin');
Route::get('/changeStatus', [App\Http\Controllers\AdminController::class, 'changeStatus'])->middleware('admin');
Route::get('/user/delete/{id}', [App\Http\Controllers\AdminController::class, 'destroy'])->middleware('admin');


Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->middleware('user');
