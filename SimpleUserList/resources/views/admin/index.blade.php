@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-white"><h3> You are logged in Admin Dashboard</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h5>Total User: {{ $count }}</h5>
                    <table class="table" id="user">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Type</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user )
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->type }}</td>
                                    <td>
                                        <input data-id="{{ $user->id }}" class="toggle-class" type="checkbox"
                                        data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" 
                                        data-off="Inactive" {{ $user->status?'checked':'' }}>
                                    </td>
                                    <td>{{ date('Y-m-d', strtotime($user->created_at))  }}</td>
                                    <td><a href="{{ URL::to('/user/delete/'.$user->id) }}" class="btn btn-danger confirm">Delete</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination justify-content-end">
                        {{ $users->links() }}
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/sweetAlertDelete.js') }}"></script>
<script src="{{ asset('js/statusChange.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
